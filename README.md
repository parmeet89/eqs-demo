# EQS-DevOps Engineer Test

This is the solution for the techincal evaluation test for **EQS Group AG.**
Below are the requirements for the evaluation:

    - Deploy 3 Jenkins instance on the provided instance using docker.
    - Using the IAC principles create a CI-CD pipeline to deploy a single node
    kubernetes.
    - Ensure that the pipelines are maintained in git.
    - Create 3 simple niginx based project which is to be deployed on kubernetes cluster. Ensure that the liveliness and readiness probes are configured. Any change in Git should result in the pipeline trigger and deployed after running through the stages in the kubernetes node.
    - Using config map configure the nginx configuration, for the above deployed service.

## Solution

Tools and services used for the implemetation:

- 2 AWS EC2 Ubuntu Instances (One Master and one slave)
- Docker: Docker is used to deploy a Jenkins container and with the Kubernetes cluster
- Docker-Hub: Cloud based docker repository to host our docker images
- Kubernetes: A Docker containers orchestrator
- Ansible: A python based Configuration management tool
- Jenkins: An Open source automation server used to create and deploy Docker containers inside Kubernetes.
- Bitbucket: Version control repository for code management
- VS Code: IDE for development

### Branch Structure

I have divided the solution in 3 branches described in below manner:

- **jenkins-install**: First step is to download and install Docker. Then create a Dockerfile that will run the Jenkins as a docker container with the required plugins and to make this persistent, I have used *Docker Volume*
- **ansible-k8**: This branch hosts the ansible code to create the Kubernetes infrastructure. 
- **nginx-eqs**: This branch hosts the nginx application code as well as ansible code that will deploy the application chanes to the Kubernetes cluster.

#### Jenkins Installation

First checkout to the **jenkins-install** branch. 

SSH to the server and install docker on the machine if it is not installed already [Installation Directions](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)

Pull the code and cd to the **jenkins-install** directory
Here we have two files:

* **Dockerfile** - This is the dockerfile for the Jenkins installation, here we are also setting the username and password for the Jenkins and we are passing the password as a parameter to jenkins build. This will also install the ansibel to run the ansible playbook
* **plugins.txt** - This file contains all the plugins we want to install with jenkins

Now build the image with tag:

```sh
$ docker build -t <Tag_Name> /path/to/Dockerfile
```
Create a volume and attach it to docker container. This will make our Jenkins installtion persitant and reusable

```sh
$ docker volume create <volume-name>
```
Now we will create the Jenkins Docker continer:

```sh
docker container run -d -p <host-port>:<container-port> -v <volume-name>:/var/jenkins_home -e JENKINS_PASS='<desired-jenkins-pwd>'  --name <container-name> <Tag_Name>

```
Now we can check the Jenkins Dashboard at http://<host-ip>:<host-port>

Inside Jenkins, we can set our pipeline jobs

#### Kubernetes Installation

First checkout to the **ansible-k8** branch.
Below is the directory structure:

* **001-kube-install**: in this folder we have a Jenkinsfile to create our job that will take the ***kube-dependencies.yml*** playbook. This playbook will install the common packages for master and salve kubernetes nodes
* **001-kube-master**: in this folder we have a Jenkinsfile to create our job that will take the ***kube-master.yml*** playbook. This playbook will initialize the pod cluster and add the pod network.
* **001-kube-worker**: in this folder we have a Jenkinsfile to create our job that will take the ***kube-worker.yml*** playbook. This playbook will add the pod slave to the kubernetes network
* **inventory.ini**: this file contains the address of master and slave node

#### Nginx Application

Checkout to the **nginx-eqs** branch.
Below is the directory structure:

* **Jenkinsfile** - This Jenkinsfile is used to create a pipeline job that will trigger the ansible playbook to deploy the code to the jenkins cluster
* **nginx-eqs** - this folder contains the Dockerfile that will copy the ***index.html*** file to the nginx container.
* **ansible-deployment** - this folder contians the ***kube-deployment-playbook*** that will deploy the code to the k8-cluster. This playbook will take the deployment yaml template and copy it to the master node and apply it



